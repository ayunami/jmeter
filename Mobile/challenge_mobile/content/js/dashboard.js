/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.8393333333333334, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.985, 500, 1500, "GET buyer/product/ID"], "isController": false}, {"data": [0.9275, 500, 1500, "GET buyer/order"], "isController": false}, {"data": [0.4875, 500, 1500, "GET buyer/product"], "isController": false}, {"data": [0.7425, 500, 1500, "POST seller/product"], "isController": false}, {"data": [0.79, 500, 1500, "GET seller/product"], "isController": false}, {"data": [0.91, 500, 1500, "POST auth/login as buyer"], "isController": false}, {"data": [0.8, 500, 1500, "DELETE seller/product/ID"], "isController": false}, {"data": [0.9225, 500, 1500, "POST buyer/order"], "isController": false}, {"data": [0.7075, 500, 1500, "POST auth/register as seller"], "isController": false}, {"data": [0.935, 500, 1500, "PUT buyer/order/ID"], "isController": false}, {"data": [0.86, 500, 1500, "GET seller/product/ID"], "isController": false}, {"data": [0.8325, 500, 1500, "POST auth/login as seller"], "isController": false}, {"data": [0.795, 500, 1500, "POST auth/register as buyer"], "isController": false}, {"data": [0.9725, 500, 1500, "GET buyer/order/ID"], "isController": false}, {"data": [0.9225, 500, 1500, "DELETE buyer/order/ID"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 3000, 0, 0.0, 517.8523333333354, 254, 2248, 424.0, 1102.9, 1223.9499999999998, 1422.9799999999996, 37.18163227365681, 455.11848982540124, 11.291500123938773], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["GET buyer/product/ID", 200, 0, 0.0, 312.335, 254, 1074, 280.0, 356.70000000000005, 409.5499999999999, 1053.6900000000003, 2.7464982147761603, 2.3495433946717936, 0.9762942872837134], "isController": false}, {"data": ["GET buyer/order", 200, 0, 0.0, 403.5649999999998, 273, 1115, 378.0, 525.7, 584.6499999999999, 1083.6400000000003, 2.7454048785844694, 3.225314520446403, 0.9544571648203818], "isController": false}, {"data": ["GET buyer/product", 200, 0, 0.0, 1220.5349999999999, 1036, 1822, 1189.0, 1362.9, 1411.0, 1747.0, 2.7085590465872156, 471.965686471594, 1.02893502843987], "isController": false}, {"data": ["POST seller/product", 200, 0, 0.0, 635.5699999999997, 292, 2248, 494.0, 1246.5, 1355.7499999999998, 2179.470000000003, 2.7203852065452465, 1.7879094179735848, 0.0], "isController": false}, {"data": ["GET seller/product", 200, 0, 0.0, 496.5550000000003, 285, 1149, 485.5, 628.8, 686.9, 1110.94, 2.7329871549603717, 1.9056179967204152, 0.9581468638972396], "isController": false}, {"data": ["POST auth/login as buyer", 200, 0, 0.0, 438.1199999999998, 327, 710, 422.5, 556.8, 596.6999999999999, 682.8800000000001, 2.7365770893766075, 1.3549263518690822, 1.1422002421870723], "isController": false}, {"data": ["DELETE seller/product/ID", 200, 0, 0.0, 471.56000000000006, 286, 1164, 451.0, 645.2, 695.8, 1103.920000000001, 2.757973992305253, 0.8672535405491126, 1.0423202490450516], "isController": false}, {"data": ["POST buyer/order", 200, 0, 0.0, 409.00500000000017, 283, 689, 391.0, 558.9, 619.75, 666.9000000000001, 2.7421676835538493, 1.8182928292315075, 1.1573875711249741], "isController": false}, {"data": ["POST auth/register as seller", 200, 0, 0.0, 690.9449999999998, 332, 1790, 505.5, 1324.7, 1523.6499999999999, 1756.92, 2.6691222591450803, 1.5404797413620532, 0.0], "isController": false}, {"data": ["PUT buyer/order/ID", 200, 0, 0.0, 394.28000000000003, 277, 1189, 354.0, 547.9, 654.7999999999997, 872.98, 2.7522809528396657, 1.8115599240370457, 1.1019874908830694], "isController": false}, {"data": ["GET seller/product/ID", 200, 0, 0.0, 454.73000000000013, 278, 1190, 434.0, 628.2, 699.8, 903.7300000000012, 2.7363524421945544, 1.9026200574634011, 0.9753600013681761], "isController": false}, {"data": ["POST auth/login as seller", 200, 0, 0.0, 531.435, 327, 1418, 445.0, 1089.7, 1238.2999999999997, 1380.7000000000003, 2.7003307905218388, 1.3422542698980626, 1.1312909268885438], "isController": false}, {"data": ["POST auth/register as buyer", 200, 0, 0.0, 529.6599999999996, 331, 1504, 476.5, 655.7, 1162.1999999999996, 1477.4800000000005, 2.734519203161104, 1.5755530565088394, 0.0], "isController": false}, {"data": ["GET buyer/order/ID", 200, 0, 0.0, 372.3099999999997, 272, 1034, 357.0, 464.9, 516.5999999999999, 652.9100000000001, 2.748989746268246, 3.224156919207191, 0.9718108282706105], "isController": false}, {"data": ["DELETE buyer/order/ID", 200, 0, 0.0, 407.1799999999999, 274, 1142, 376.5, 548.5, 614.75, 1085.0400000000009, 2.7558838119384887, 0.8881266190817395, 1.0334564294769333], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 3000, 0, "", "", "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
